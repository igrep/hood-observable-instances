{-# OPTIONS_GHC -fno-warn-orphans #-}

module Debug.Hood.Observe.Instances where


import qualified Data.Foldable as F
import           Data.Hashable (Hashable)
import qualified Data.HashMap.Lazy as HML
import qualified Data.IntMap as IM
import qualified Data.List.NonEmpty as NE
import qualified Data.Sequence as S
import qualified Data.Text as T
import           Data.Tree (Tree(..))
import           Debug.Hood.Observe
import           GHC.Generics (Generic)


instance (Eq k, Hashable k, Observable k, Observable v) => Observable (HML.HashMap k v) where
  observer hm = send "HashMap.fromList" (return HML.fromList << HML.toList hm)

instance Observable v => Observable (IM.IntMap v) where
  observer im = send "IntMap.fromList" (return IM.fromList << IM.toList im)

instance Observable a => Observable (S.Seq a) where
  observer s = send "Seq.fromList" (return S.fromList << F.toList s)

instance Observable a => Observable (NE.NonEmpty a)

instance Observable T.Text where { observer = observeBase }

deriving instance Generic (Tree a)
instance Observable a => Observable (Tree a)
