import qualified Data.Foldable as F
import qualified Data.HashMap.Lazy as HML
import qualified Data.IntMap as IM
import qualified Data.List.NonEmpty as NE
import qualified Data.Sequence as S
import qualified Data.Text as T
import           Data.Tree (Tree(Node))
import           Debug.Hood.Observe
import           Debug.Hood.Observe.Instances ()


main :: IO ()
main = do
  printO $ observe "the tuple"
    ( lengthO $ HML.fromList [("abc", "def"), ("123", "456")]
    , lengthO $ IM.fromList [(1, T.pack "def"), (2, T.pack "456")]
    , lengthO $ S.fromList ['a'..'z']
    , lengthO $ NE.fromList ([1..10] :: [Integer])
    , lengthOWith T.length $ T.replicate 2048 $ T.pack "0"
    )
  printO $ observe "tree" $ Node 'a' [Node 'b' []]


lengthO :: (F.Foldable f, Observable a, Observable (f a)) => f a -> Int
lengthO = lengthOWith F.length

lengthOWith :: Observable a => (a -> Int) -> a -> Int
lengthOWith caluculateLength =
  observe "length" (observe "caluculateLength" caluculateLength)
